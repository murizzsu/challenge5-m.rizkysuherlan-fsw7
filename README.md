# Car Management Dashboard
Webite manajemen data mobil yang didalamnya kita bisa membuat data mobil baru, menghapus, update ataupun delete. 

## How to run
1. Pasti modul sudah terinstal, untuk installasi ketikan perintah 
>>yarn add multer express ejs
2. Inisialisai Sequelize dengan perintah 
>>yarn sequelize-cli init
3. Lakukan edit database pada folder config>config.json
4. Buat database
>>yarn sequelize-cli db:create <br />
>>yarn sequelize-cli model:generate --name Sizes --attributes name:string --underscored <br />
>>yarn sequelize-cli model:generate --name Cars --attributes name:string,price:integer,size_id:integer,photo:string --underscored <br />
>>yarn migrate <br />
## Endpoints
Pada index.js kita bisa melihat beberapa endpoint, berikut ini fungsi-fungsinya
>>app.get("/") : menampilkan ke halaman awal yang berisikan data mobil-mobil yang telah dibuat <br />
>>app.get("form") : akan menampilkan form halaman create mobil <br />
>.app.get("form.:id") : menampilkan form edit mobil<br />
>>app.get("/api/v1/cars") : menampilkan semua data mobil, hasilnya akan berupa object<br />
>>app.get("/api/v1/cars/:id") : menampilkan data mobil berdasarkan id tertentu.<br />
>>app.post("/api/v1/cars") : create data mobil<br />
>>app.post("/api/v1/cars-upload") : create file
>app.put("/api/v1/cars/:id") : melakukan edit data mobil<br />
>>app.delete("/api/v1/cars/:id") : menghapus data mobil berdasarkan id tertentu <br />


## Directory Structure

```
.gitignore
├── config
│   └── config.json
├── controllers
│   └── car_delete.js
│   └── car_get_all.js
│   └── car_get_id.js
│   └── car_post.js
│   └── car_put.js
│   └── car_upload.js
│   └── form.js
│   └── index.js
│   └── list.js
├── migrations
├── models
│   └── cars.js
│   └── index.js
│   └── sizes.js
├── node_modules
├── public
│   ├── css
│   ├── img
│   ├── js
├── seeders
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   │   └── navbar.ejs
│   │   └── scripts.ejs
│   │   └── sidebar.ejs
│   ├── form.ejs
│   └── list.ejs
├── index.js
├── package.json
├── README.md
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/img/ERD.png)
