const express = require("express");
const app = express();
const multer = require("multer");
const form = multer({ dest: "public/img" });
const controllers = require("./controllers");
const { PORT = 8000 } = process.env;
const fs = require("fs");
app.set("view engine", "ejs");
app.use(express.json());

app.use("/assets", express.static("public"));
app.get("/form", controllers.form);
app.get("/form/:id", controllers.form);
app.get("/api/v1/cars", controllers.carGetAll);
app.get("/api/v1/cars/:id", controllers.carGetId);
app.post("/api/v1/cars", controllers.carPost);
app.post("/api/v1/car_upload", form.single("photo"), controllers.carUpload);
app.get("/", controllers.list);
app.delete("/api/v1/cars/:id", controllers.carDelete);
app.put("/api/v1/cars/:id", controllers.carPut);
app.listen(PORT, () => {
  `listen on port ${PORT}`;
});
