const fs = require("fs");
module.exports = (req,res)=>{
    let result = {}
    if(req.file){
        let originalname = req.file.originalname
        let temp = req.file.path
        fs.copyFileSync(temp,`public/img/${originalname}`)
        fs.unlinkSync(temp)
        result.url = `${originalname}`
    }
    res.json(result)
}

