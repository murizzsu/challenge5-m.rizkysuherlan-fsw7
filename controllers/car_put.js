const { Cars } = require("../models");

async function carPut(req, res) {
  let params = req.params.id;
  let temp = [];
  let splitId = params.split("");

  for (let i of splitId) {
    if (i != ":") {
      temp.push(splitId[i]);
    }
  }
  let carId = temp.join("");

  await Cars.update(
    {
      name: req.body.name,
      price: req.body.price,
      size_id: req.body.size_id,
      photo: req.body.photo,
    },
    { where: { id: carId } }
  );

  res.send(`Data berhasil diupdate `);
}

module.exports = carPut;
