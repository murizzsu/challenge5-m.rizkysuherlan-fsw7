const { Cars } = require("../models");
const fs = require("fs");

module.exports = (req, res) => {
  let car = Cars.create({
    name: req.body.name,
    price: req.body.price,
    size_id: req.body.size_id,
    photo: req.body.photo,
  });
  res.json(car);
};
