const { Cars } = require("../models");
async function form(req, res) {
  let id = req.params.id;
  let a = false;
  let carFind = null;
  if (id) {
    let car = await Cars.findOne({ where: { id: id } });
    carFind = car;
    a = true;
  } else {
    a = false;
  }

  let title = "Add New Car";
  let name, price, size_id, photo;
  if (a) {
    title = "Update Car Information";
    name = carFind.name;
    price = carFind.price;
    size_id = carFind.size_id;
    photo = carFind.photo;
  }

  res.render("form", {
    title: title,
    id: id,
    name: name,
    price: price,
    size_id: size_id,
    photo: photo,
  });
}

module.exports = form;
