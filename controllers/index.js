module.exports = {
  list: require("./list"),
  form: require("./form"),
  carPost: require("./car_post"),
  carGetAll: require("./car_get_all"),
  carUpload: require("./car_upload"),
  carDelete: require("./car_delete"),
  carPut: require("./car_put"),
  carGetId: require("./car_get_id"),
};
